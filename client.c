
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include<time.h>
#include "lib.h"

MancalaBoard* board;
int alwaysPrintBoard = 1;
int p1First = 1;
int _2player = 1;
int *b;

void clrscr()
{
    system("@cls||clear");
}


void die_with_error(char *error_msg){
    printf("%s", error_msg);
    exit(-1);
}

void printBoard() {
    clrscr();
        int* b = board->board;
        printf("\n\n\n\n\n\t\t--MANCALA GAME-- \n");
        printf("\n\n\n\n");
        printf("         6     5     4     3     2     1\n");
        printf("-------------------------------------------------\n");
        printf("|%5d|%5d|%5d|%5d|%5d|%5d|%5d|%5s|\n", b[6], b[5], b[4], b[3], b[2], b[1], b[0], "");
        printf("       -----------------------------------\n");
        printf("|%5s|%5d|%5d|%5d|%5d|%5d|%5d|%5d|\n", "", b[7], b[8], b[9], b[10], b[11], b[12], b[13]);
        printf("-------------------------------------------------\n");
        printf("         1     2     3     4     5     6\n");

}





int gameOver() {
    int result = gameIsOver(board);
    if (result == NOT_OVER) {
        return 0;
    }
     else {
        if (b[14]==3) 
        {
            printf("It's a tie!\n");
        } 

        else if (b[14]==1) 
        {
            printf("Player 1 wins!\n");
        } 

        else if (b[14]==2) 
        {
            printf("Player 2 wins!\n");
        } 

    }
    return 1;
}





int main(int argc,  char *argv[]){
    
    int client_sock,  port_no,  n;
    struct sockaddr_in server_addr;
    struct hostent *server;
    char * cmd = malloc(255);
    char buffer[256];

    int* gameStat;
    board = (MancalaBoard*)malloc(sizeof(MancalaBoard));
 
    if (argc < 3) {
        printf("Usage: %s hostname port_no",  argv[0]);
        exit(1);
    }

    //printf("Client starting ...\n");
    // Create a socket using TCP
    client_sock = socket(AF_INET,  SOCK_STREAM,  0);
    if (client_sock < 0) 
        die_with_error("Error: socket() Failed.");

    //printf("Looking for host '%s'...\n", argv[1]);
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        die_with_error("Error: No such host.");
    }
    //printf("Host found!\n");

    // Establish a connection to server
    port_no = atoi(argv[2]);
    bzero((char *) &server_addr,  sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,  
         (char *)&server_addr.sin_addr.s_addr, 
         server->h_length);
         
    server_addr.sin_port = htons(port_no);

    //printf("Connecting to server at port %d...\n", port_no);
    if (connect(client_sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) 
        die_with_error("Error: connect() Failed.");

    //printf("Server succesfully connected ...\n"); 
    n = recv(client_sock, board, 255, 0);

    printBoard();

    
    int input_catch = 0;
    int player1_score =0;
    int player2_score =0;
   
    // main communication
    while(1)
    {
        int to_print = 1;
        
        if(input_catch == 0)
        {
          //bzero(b, 256);
          n = recv(client_sock, board, 255, 0);
          b = board->board;

            if (n < 0) die_with_error("Error: recv() Failed.");
        
        //printf("[server] > %s", buffer);

          printBoard();

        if (b[14] > 0) 
            {
                if (b[14]==3) 
        {
            printf("It's a tie!\n");
        } 

        else if (b[14]==1) 
        {
            printf("Player 1 wins!\n");
        } 

        else if (b[14]==2) 
        {
            printf("Player 2 wins!\n");
        } 
                break;
            }
        


          printf("\n\n\n\n");      
		  printf("Player [2] >  ");
		  bzero(buffer, 256);
		  fgets(buffer, 255, stdin);
        }

        else
        {
            printf("\n\n\n\n");
            printf("Player [2] >  ");
            bzero(buffer, 256);
            fgets(buffer, 255, stdin);
        }


        cmd = buffer;
        int position = (int)strtol(cmd, (char**)NULL, 10);
        
        position+=7;

        if (position < 8 || position > 13) 
        {
            to_print = 0;
            input_catch = 1;
            printf("Invalid input\n");
            continue;
        }

        if (b[position - 1] == 0) 
        {
            printf("No pebbles in this pocket\nChoose different pocket\n");
            input_catch = 1;
            continue;
        }


        
        else
        {
        input_catch = 0;
		n = send(client_sock, buffer, strlen(buffer), 0);
        n = recv(client_sock, board, 255, 0);
        n = send(client_sock, " ", strlen(buffer), 0);
        } 


		if (n < 0) die_with_error("Error: send() Failed.");



        
        if(to_print == 1)
            printBoard(); 

        
	}
	
    close(client_sock);
    return 0;
}
