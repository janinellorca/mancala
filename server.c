

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include<time.h>


#include "lib.h"

MancalaBoard* board;

int alwaysPrintBoard = 1;
int p1First = 1;
int _2player = 1;
int* b2;
int* gameStat;
int* b;

void clrscr()
{
    system("@cls||clear");
}


void printBoard(int currentPlayer) {
	clrscr();
	printf("\n\n\n\n\n\t\t--MANCALA GAME-- \n");
	printf("\n\n\n\n");
	b = board->board;
	b2 = b;

	printf("         6     5     4     3     2     1\n");
	printf("-------------------------------------------------\n");
	printf("|%5d|%5d|%5d|%5d|%5d|%5d|%5d|%5s|\n", b[13], b[12], b[11], b[10], b[9], b[8], b[7], "");
	printf("       -----------------------------------\n");
	printf("|%5s|%5d|%5d|%5d|%5d|%5d|%5d|%5d|\n", "", b[0], b[1], b[2], b[3], b[4], b[5], b[6]);
	printf("-------------------------------------------------\n");
	printf("         1     2     3     4     5     6\n");
}

void printBoard2(){
	clrscr();
	printf("\n\n\n\n\n--MANCALA GAME-- \n");
	b = board->board;
	b2 = b;

	printf("         6     5     4     3     2     1\n");
	printf("-------------------------------------------------\n");
	printf("|%5d|%5d|%5d|%5d|%5d|%5d|%5d|%5s|\n", b[13], b[12], b[11], b[10], b[9], b[8], b[7], "");
	printf("       -----------------------------------\n");
	printf("|%5s|%5d|%5d|%5d|%5d|%5d|%5d|%5d|\n", "", b[0], b[1], b[2], b[3], b[4], b[5], b[6]);
	printf("-------------------------------------------------\n");
	printf("         1     2     3     4     5     6\n");
}




void help() {
	printf("Help\nshow - show board\nhelp/? - show this list\n(pocket number) - select a pocket\nquit - quit game\n");
}

void die_with_error(char *error_msg)
{
    printf("%s", error_msg);
    exit(-1);
}



int gameOver() {
	int result = gameIsOver(board);
	if (result == NOT_OVER) {

		return 0;
	}
	 else {
		if (result == (P1_WINS | P2_WINS)) {
			printf("It's a tie!\n");
			b[14] = 3;
		} else {
			if (result == P1_WINS) {
				if (_2player) 
				{
					printf("Player 1 wins!\n");
					b[14] = 1;
				}
			} else {
				if (_2player) {
					printf("Player 2 wins!\n");
					b[14] = 2;
					
				} 
			}
		}
	}
	return 1;
}

void gameOver2(){
	if (b[14]==3) 
        {
            printf("It's a tie!\n");
        } 

        else if (b[14]==1) 
        {
            printf("Player 1 wins!\n");
        } 

        else if (b[14]==2) 
        {
            printf("Player 2 wins!\n");
        } 
}

 


int main(int argc, char * argv[]) {
	int pebblesChanged = 0;

	int fastMode = 0;
	int startingPebbles = 4;
	int currentPlayer = 2;


	board = (MancalaBoard*)malloc(sizeof(MancalaBoard));
	setupBoard(board, startingPebbles, fastMode);
	printBoard(currentPlayer);

	int server_sock, client_sock, port_no, client_size, n;
    char buffer[256];
    char trash[256];
    struct sockaddr_in server_addr, client_addr;
    if (argc < 2) {
        printf("Usage: %s port_no", argv[0]);
        exit(1);
    }

    //printf("Server starting ...\n");
    // Create a socket for incoming connections
    server_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock < 0) 
       die_with_error("Error: socket() Failed.");
       
    // Bind socket to a port
    bzero((char *) &server_addr, sizeof(server_addr));
    port_no = atoi(argv[1]);
    server_addr.sin_family = AF_INET; // Internet address 
    server_addr.sin_addr.s_addr = INADDR_ANY; // Any incoming interface
    server_addr.sin_port = htons(port_no); // Local port
    
    if (bind(server_sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) 
       die_with_error("Error: bind() Failed.");
       
    // Mark the socket so it will listen for incoming connections
    listen(server_sock, 5);
    //printf("Server listening to port %d ...\n", port_no);
    
    //printf("Waiting for connection(s) ...\n");

    // Accept new connection
    client_size = sizeof(client_addr);
    client_sock = accept(server_sock, (struct sockaddr *) &client_addr, &client_size);
    if (client_sock < 0) 
        die_with_error("Error: accept() Failed.");

    //printf("Client succesfully connected ...\n");  




    // main communication

	
	//printf("Closing connection ...\n");
   
    n = send(client_sock, board, 255, 0);




	if (p1First || !_2player) {
		currentPlayer = 1;
	}

	
	char * cmd = malloc(255);

	while (!gameOver()) {  		
		if(currentPlayer ==1)
		{
					printf("\n\n\n\n");
					printf("Player [%d] > ", currentPlayer);
					fgets(cmd, 255, stdin);
		}

		else
		{
		printf("Player [%d] > ", currentPlayer);
		n = send(client_sock, board, 255, 0);
		if (n < 0) die_with_error("Error: send() Failed.");
		

		bzero(buffer, 256);
		n = recv(client_sock, buffer, 255, 0);
	
		if (n < 0)die_with_error("Error: recv() Failed.");
		printf("[client error] > %s", buffer);
		cmd = buffer;		
		}
		

		if (!strncmp(cmd, "show", 4))
		{
			printBoard(currentPlayer);
		} 

		else if (!strncmp(cmd, "quit", 4)) 
		{
			
		} 
		else if (!strncmp(cmd, "help", 4) || cmd[0] == '?') 
		{
			help();
		} 
		else 
		{
			if (cmd[0] == '\n') 
			{
				continue;
			}

			int position = (int)strtol(cmd, (char**)NULL, 10);


			if (position < 1 || position > 6) 
			{

					printf("Invalid input\n");

				continue;
			}

		position--;
			
			if (currentPlayer == 2) 
			{
				position += 7;
			}
			if (board->board[position] == 0) 
			{
				printf("No pebbles in this pocket\nChoose different pocket\n");
				continue;
			}

		int result = movePocket(board, position, currentPlayer == 1 ? MANCALA_GOAL1 : MANCALA_GOAL2);
			
			if (alwaysPrintBoard) 
			{
				printBoard(currentPlayer);

			}

			 

			else if (result == MOVE_CAPTURE)
			{
				printf("Capture!\n");
			}

			if (gameOver()) 
			{
				n = send(client_sock, board, 255, 0);
				n = recv(client_sock, trash, 255, 0);
				printBoard(currentPlayer);
				gameOver2();
				break;
			}

			if (currentPlayer == 1 && _2player) 
			{
				currentPlayer = 2;
			} 


			else if (currentPlayer == 2) 
			{
				currentPlayer = 1;
				n = send(client_sock, board, 255, 0);
				n = recv(client_sock, trash, 255, 0);
			}
		}
		memset(cmd, 0, 255);
	}
close(client_sock);
close(server_sock);
return 0;
	

	/*free(cmd);
	if (!alwaysPrintBoard) {
		printBoard(currentPlayer);
	}
	free(board); */

}
